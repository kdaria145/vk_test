import React, {FC, useEffect} from 'react';
import {useAppDispatch, useAppSelector} from "./app/hooks";
import {
  selectGeolocationPermission,
  setGeolocation,
  setGeolocationPermission
} from "./store/weatherSlice";
import WeatherWrapper from "./wrappers/Weather";
import {Alert} from "antd";
import {cn} from "./utils/bem-config";
import './App.scss'

const bem = cn('app');

const App:FC = () => {
  const dispatch = useAppDispatch();
  const geolocationPermission = useAppSelector(selectGeolocationPermission);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(async (position) => {
      console.log('position', position)
      const locationObj = {lat: position?.coords?.latitude, lon: position?.coords?.longitude}
      await dispatch(setGeolocationPermission(true))
      await dispatch(setGeolocation(locationObj))
    }, async (error) => {
      await dispatch(setGeolocationPermission(false))
    });
  }, [])

  if(!geolocationPermission) {
    return <Alert
        message="Пожалуйста, разрешите доступ к геоданным"
        description="Пожалуйста, разрешите доступ к геоданным"
        type="error"
    />
  }

  return (
    <div className={bem()}>
      <WeatherWrapper/>
    </div>
  );
}

export default App;
