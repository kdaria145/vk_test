export type Geolocation = {
    lat: number;
    lon: number;
}

export interface Info {
    lat: number;
    lon: number;
    url: string;
    tzinfo: {
        name: string;
    }
}

export interface Fact {
    temp: number;
    feels_like: number;
    icon: string;
    condition: string;
    wind_speed: number;
    wind_gust: number;
    wind_dir: string;
    pressure_mm: number;
    pressure_pa: number;
    humidity: number;
    daytime: string;
    polar: boolean;
    season: string;
    obs_time: number;
}

export interface Part {
    part_name: string;
    temp_min: number;
    temp_max: number;
    temp_avg: number;
    feels_like: number;
    icon: string;
    condition: string;
    daytime: string;
    polar: boolean;
    wind_speed: number;
    wind_gust: number;
    wind_dir: string;
    pressure_mm: number;
    pressure_pa: number;
    humidity: number;
    prec_mm: number;
    prec_period: number;
    prec_prob: number;
}

export interface Forecast {
    date: string;
    date_ts: number;
    week: number;
    sunrise: string;
    sunset: string;
    moon_code: number;
    moon_text: string;
    parts: {
        day: Part,
        night: Part
    };
}

export interface IWeather {
    now: number;
    now_dt: Date;
    info: Info;
    fact: Fact;
    forecasts: Array<Forecast>;
}