import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Geolocation} from "../types/weatherTypes";
import {RootState} from "../app/store";
import {getPlaces, savePlaces} from "../utils/placesLocalStorage";

type WeatherState = {
    places: Array<Geolocation & {name: string}> | null,
    targetPlace: Geolocation & {name: string} | null
};

const initialState: WeatherState = {
    places: getPlaces() || null,
    targetPlace: getPlaces() || null
};


const PlacesSlice = createSlice({
    name: 'weather',
    initialState,
    reducers: {
        addPlace(state, action: PayloadAction<Geolocation & {name: string}>) {
            const isHasTheSamePlace = state.places?.findIndex(place => Math.trunc(place.lat) === Math.trunc(action.payload.lat)
                && Math.trunc(place.lon) === Math.trunc(action.payload.lon))
            if(isHasTheSamePlace == undefined || isHasTheSamePlace < 0) {
                state.places = state.places === null ? [action.payload] : [...state.places, action.payload];
                savePlaces({places: state.places})
            }
        },
        deletePlace(state, action: PayloadAction<Geolocation & {name: string}>) {
            if(state.places === null) {
                return;
            }
            const theDamePlaceIndex = state.places?.findIndex(place => Math.trunc(place.lat) === Math.trunc(action.payload.lat)
                && Math.trunc(place.lon) === Math.trunc(action.payload.lon))
            console.log('theDamePlaceIndex', theDamePlaceIndex)
            if(theDamePlaceIndex && theDamePlaceIndex > 0) {
                let newPlaces = state.places;
                delete newPlaces[theDamePlaceIndex];
                state.places = newPlaces
                savePlaces({places: newPlaces})
            }
        },
        setTargetPlace(state, action: PayloadAction<Geolocation & {name: string}>) {
            state.targetPlace = action.payload
        }
    },
});

export const selectPlaces = (state: RootState): Array<Geolocation & {name: string}> | null => state.places.places;
export const selectTargetPlace = (state: RootState): Geolocation & {name: string} | null => state.places.targetPlace;

export const { addPlace, deletePlace } = PlacesSlice.actions;

export default PlacesSlice.reducer;
