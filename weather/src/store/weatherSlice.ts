import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AxiosResponse} from "axios";
import {axiosInstance} from "../api/axiosInstanse";
import {Geolocation, IWeather} from "../types/weatherTypes";
import {RootState} from "../app/store";

type WeatherState = {
    weather: IWeather | null,
    geolocationPermission: boolean,
    userGeolocation: Geolocation | null,
    pageIsLoading: boolean,
};

const initialState: WeatherState = {
    weather: null,
    geolocationPermission: true,
    userGeolocation: null,
    pageIsLoading: false
};

export const getWeather = createAsyncThunk(
    'getWeather',
    async ({lat, lon}: Geolocation) => {
        const response: AxiosResponse<IWeather> = await axiosInstance.get(`/weather?lat=55.3333&lon=86.0833`);
        //TODO: Получение по кординатам
        return response.data;
    },
);


const WeatherSlice = createSlice({
    name: 'weather',
    initialState,
    reducers: {
        setGeolocationPermission(state, action: PayloadAction<boolean>) {
            state.geolocationPermission = action.payload;
        },
        setGeolocation(state, action: PayloadAction<Geolocation>) {
            state.userGeolocation = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(getWeather.pending, (state, action) => {
            state.pageIsLoading = true
        })
        builder.addCase(getWeather.fulfilled, (state, action) => {
            state.pageIsLoading = false
            state.weather = action.payload
        })
    }
});

export const selectWeather = (state: RootState): IWeather | null => state.weather.weather;
export const selectGeolocationPermission = (state: RootState): boolean => state.weather.geolocationPermission;
export const selectUserGeolocation = (state: RootState): Geolocation | null => state.weather.userGeolocation;
export const selectPageIsLoading = (state: RootState): boolean => state.weather.pageIsLoading;

export const { setGeolocationPermission, setGeolocation } = WeatherSlice.actions;

export default WeatherSlice.reducer;
