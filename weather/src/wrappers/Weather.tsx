import {FC, useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import {getWeather, selectPageIsLoading, selectUserGeolocation, selectWeather} from "../store/weatherSlice";
import Weather from "../components/Weather/Weather";
import {Spin} from "antd";
import {addPlace, selectTargetPlace} from "../store/placesSlice";

const WeatherWrapper:FC = () => {
    const dispatch = useAppDispatch()
    const userGeolocation = useAppSelector(selectUserGeolocation);
    const targetPlace = useAppSelector(selectTargetPlace);
    const pageIsLoading = useAppSelector(selectPageIsLoading);
    const weather = useAppSelector(selectWeather);

    useEffect(() => {
        if(userGeolocation !== null && targetPlace !== null) {
            dispatch(getWeather(targetPlace))
            dispatch(addPlace({...userGeolocation, name: weather?.info.tzinfo?.name || ''}))
        }
    }, [userGeolocation])

    if(pageIsLoading) {
        return <Spin/>
    }
    console.log('weather', weather)
    return (
        <Weather weather={weather}/>
    )
}
export default WeatherWrapper