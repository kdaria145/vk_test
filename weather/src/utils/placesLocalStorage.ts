import {Geolocation} from "../types/weatherTypes";

type ArgumentsType = {
    places: Array<Geolocation>
}

export const getPlaces = () => {
    return JSON.parse(<string>localStorage.getItem('places'));
}

export const savePlaces = ({places}: ArgumentsType) => {
    localStorage.setItem('places', JSON.stringify(places));
}