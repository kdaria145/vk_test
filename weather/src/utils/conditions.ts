import IconClear from '../assets/conditions/clear.png'
import IconCloudy from '../assets/conditions/cloudy.png'
import IcondDrizzle from '../assets/conditions/drizzle.png'
import IconHail from '../assets/conditions/hail.png'
import IconHeavyRain from '../assets/conditions/heavy-rain.png'
import IconOvercast from '../assets/conditions/overcast.png'
import IconPartyCloudy from '../assets/conditions/partlyCloudy.png'
import IconRain from '../assets/conditions/rainy.png'
import IconLightRain from '../assets/conditions/light-rain.png'
import IconModerateRain from '../assets/conditions/moderate-rain.png'
import IconShower from '../assets/conditions/shower.png'
import IconWetShow from '../assets/conditions/wet-snow.png'
import IconLightShow from '../assets/conditions/light-snow.png'
import IconSnow from '../assets/conditions/snow.png'
import IconSnowShowers from '../assets/conditions/snow-showers.png'
import IconThunderStorm from '../assets/conditions/thunderstorm.png'
import IconThunderStormWithRain from '../assets/conditions/thunderstorm-with-rain.png'
import IconСontinuousHeavyRain from '../assets/conditions/continuous-heavy-rain.png'
import IconThunderstormWithHail from '../assets/conditions/thunderstorm-with-hail.png'


export const conditions = new Map( [
  ['clear','Ясно'],
  ['partly-cloudy' , 'Малооблачно'],
  ['cloudy' , 'Облачно с прояснениями'],
  ['overcast' , 'Пасмурно'],
  ['drizzle' , 'Морось'],
  ['light-rain' , 'Небольшой дождь'],
  ['rain' , 'Дождь'],
  ['moderate-rain' , 'Умеренно сильный дождь'],
  ['heavy-rain' , 'Сильный дождь'],
  ['continuous-heavy-rain' , 'Длительный сильный дождь'],
  ['showers' , 'Ливень'],
  ['wet-snow' , 'Дождь со снегом'],
  ['light-snow', 'Небольшой снег'],
  ['snow' ,'Снег'],
  ['snow-showers' , 'Снегопад'],
  ['hail' , 'Град'],
  ['thunderstorm' , 'Гроза'],
  ['thunderstorm-with-rain' , 'Дождь с грозой'],
  ['thunderstorm-with-hail' , 'Гроза с градом'],
])
export const iconsConditions = new Map([
  ['clear',IconClear],
  ['partly-cloudy' , IconPartyCloudy],
  ['cloudy' , IconCloudy],
  ['overcast' , IconOvercast],
  ['drizzle' , IcondDrizzle],
  ['light-rain' , IconLightRain],
  ['rain' , IconRain],
  ['moderate-rain' , IconModerateRain],
  ['heavy-rain' , IconHeavyRain],
  ['continuous-heavy-rain' , IconСontinuousHeavyRain],
  ['showers' , IconShower],
  ['wet-snow' , IconWetShow],
  ['light-snow', IconLightShow],
  ['snow' ,IconSnow],
  ['snow-showers' , IconSnowShowers],
  ['hail' , IconHail],
  ['thunderstorm' , IconThunderStorm],
  ['thunderstorm-with-rain' , IconThunderStormWithRain],
  ['thunderstorm-with-hail' , IconThunderstormWithHail],
])