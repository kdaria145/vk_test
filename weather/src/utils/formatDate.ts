const days = [
    'Воскресенье', 'Понедельник', 'Вторник',
    'Среда', 'Четверг', 'Пятница', 'Суббота',
]
const months = [
    'Января', 'Февраля', 'Марта', 'Апреля',
    'Мая', 'Июня', 'Июля', 'Августа',
    'Сентября', 'Октября', 'Ноября', 'Декабря',
]

export const formatDate = (date: string) => {
    const dateToFormat = new Date(date)
    const day = days[dateToFormat.getDay()]
    const d = dateToFormat.getDate()
    const m = months[dateToFormat.getMonth()]
    const y = dateToFormat.getFullYear()
    return `${day}, ${d} ${m} ${y}`
}