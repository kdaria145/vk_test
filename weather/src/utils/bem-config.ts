import { withNaming } from '@bem-react/classname';

export const cn = withNaming({
  n: 'weather-', e: '__', m: '--', v: '_',
});
