import {FC, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../app/hooks";
import {Alert, Row, Col, Button, Modal, Form, InputNumber, Input, List} from "antd";
import PlaceInfo from "../PlaceInfo/PlaceInfo";
import WeatherList from "../WeatherList/WeatherList";
import {Geolocation, IWeather} from "../../types/weatherTypes";
import {addPlace, deletePlace, selectPlaces} from "../../store/placesSlice";

type PropType = {
    weather: IWeather | null
}

const Weather: FC<PropType> = ({weather}: PropType) => {
    const dispatch = useAppDispatch()
    const places = useAppSelector(selectPlaces)
    const [isModalOpen, setIsModalOpen] = useState(false);

    if (weather === null) {
        return <Alert
            message="Нет информации о погоде в данном месте"
            type="warning"
        />
    }

    const showModal = () => {
        setIsModalOpen(true);
    };

    const onFinish = (values: Geolocation & { name: string }) => {
        dispatch(addPlace(values))
        setIsModalOpen(false);
    };

    const onFinishFailed = (errorInfo: any) => {
        setIsModalOpen(false);
    };

    return (
        <>
            <Row>
                <Col span={8} offset={16}>
                    <PlaceInfo {...weather.info}/>
                </Col>
                <Col span={8} offset={0}>
                    <Button type="primary" onClick={showModal}>Добавить новое место</Button>
                </Col>
                <Modal title="Добавление места" open={isModalOpen} footer={null} onCancel={() => setIsModalOpen(false)}>
                    <Form
                        name="basic"
                        labelCol={{span: 8}}
                        wrapperCol={{span: 16}}
                        initialValues={{remember: true}}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Долгота"
                            name="lat"
                            rules={[{required: true, message: 'Пожалуйста, введите долготу!'}]}
                        >
                            <InputNumber/>
                        </Form.Item>
                        <Form.Item
                            label="Ширина"
                            name="lon"
                            rules={[{required: true, message: 'Пожалуйста, введите ширину!'}]}
                        >
                            <InputNumber/>
                        </Form.Item>
                        <Form.Item
                            label="Название"
                            name="name"
                            rules={[{required: true, message: 'Пожалуйста, введите название!'}]}
                        >
                            <Input/>
                        </Form.Item>
                        <Form.Item wrapperCol={{offset: 8, span: 16}}>
                            <Button type="primary" htmlType="submit">
                                Сохранить место
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
                {/*<List*/}
                {/*    itemLayout="horizontal"*/}
                {/*    dataSource={places || []}*/}
                {/*    renderItem={(item) => (*/}
                {/*        <List.Item key={item.lon + item.lat}*/}
                {/*            actions={[<a>Редактировать</a>, <Button onClick={() => dispatch(deletePlace(item))}>Удалить</Button>]}*/}
                {/*        >*/}
                {/*            {item.lon}*/}
                {/*            {item.lat}*/}
                {/*        </List.Item>)}*/}
                {/*/>*/}
                {/*TODO: починить список*/}

                </Row>
            <WeatherList forecasts={weather.forecasts}/>
        </>
    )
}
export default Weather