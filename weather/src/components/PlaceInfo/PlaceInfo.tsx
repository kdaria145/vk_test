import {FC} from "react";
import {Table, Typography} from 'antd'
import {Info} from "../../types/weatherTypes";
import {ColumnsType} from "antd/es/table";

const PlaceInfo:FC<Info> = (info: Info) => {
    console.log('info',info)
    type DataForTable = {lat: number, lon: number}

    const data: DataForTable[] = [{lat: info.lat, lon: info.lon}];

    const columnsNames: ColumnsType<DataForTable> = [
        {
            title: 'Ширина',
            dataIndex: 'lat',
        },
        {
            title: 'Долгота',
            dataIndex: 'lon',
        },
        // {
        //     title: 'Название',
        //     dataIndex: 'name',
        // },
    ];
    return (
        <>
            <Typography.Paragraph>Информация о месте</Typography.Paragraph>
            <Table columns={columnsNames} dataSource={data} pagination={false}/>
        </>
    )
}
export default PlaceInfo