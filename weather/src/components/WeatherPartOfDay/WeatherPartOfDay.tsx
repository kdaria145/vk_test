import React, {FC} from "react";
import {Part} from "../../types/weatherTypes";
import {conditions, iconsConditions} from "../../utils/conditions";
import {cn} from "../../utils/bem-config";
import './WeatherPartOfDay.scss'
import {Col, Row, Typography} from "antd";

type PropsType = {
    partOfDay: Part | null,
    title: string
}

const bem = cn('part-of-date');

const WeatherPartOfDay: FC<PropsType> = ({partOfDay, title}: PropsType) => {
    if (partOfDay === null) {
        return null
    }
    const {condition, temp_max, temp_min, feels_like, wind_speed} = partOfDay
    return (
        <div className={bem()}>
            <Typography.Title level={5}>{title}</Typography.Title>
            <div className={bem('condition')}><img src={iconsConditions.get(condition)} style={{height:'40px', width: '40px'}} alt=""/></div>
            {conditions.get(condition)}
            <div className={bem('temp')}>
                <Typography.Title level={5}>{temp_max}</Typography.Title>
                <Typography.Paragraph>{temp_min}</Typography.Paragraph>
            </div>
            <Typography.Paragraph>Ощущается как: {feels_like}</Typography.Paragraph>
            <Typography.Paragraph>Скорость ветра: {wind_speed} м/с</Typography.Paragraph>
        </div>
    )
}
export default WeatherPartOfDay