import React, { FC } from 'react'

import {cn} from "../../utils/bem-config";
import {Forecast} from "../../types/weatherTypes";
import {Typography} from "antd";
import WeatherPartOfDay from "../WeatherPartOfDay/WeatherPartOfDay";
import {Row, Col} from 'antd'
import {formatDate} from "../../utils/formatDate";
import './WeatherCard.scss'

const bem = cn('weather-card');

type PropsType = {
    forecast?: Forecast
}

const WeatherCard:FC<PropsType> = ({forecast}: PropsType) => {
    if(!forecast) {
        return null
    }
    return (
        <div className={bem()}>
            <Typography.Title level={4} style={{ margin: 0 }}>{formatDate(forecast.date)}</Typography.Title>
            <Row className={bem('parts')}>
                <Col span={12}><WeatherPartOfDay partOfDay={forecast?.parts?.day || null} title={'День'}/></Col>
                <Col span={12}><WeatherPartOfDay partOfDay={forecast?.parts?.night || null} title={'Ночь'}/></Col>
            </Row>
        </div>
    )
}

export default WeatherCard