import React, {FC} from "react";
import {Forecast} from "../../types/weatherTypes";
import {List, Typography, Tabs} from "antd";
import WeatherCard from "../WeatherCard/WeatherCard";
import {formatDate} from "../../utils/formatDate";

type PropsType = {
    forecasts?: Array<Forecast>
}

const WeatherList:FC<PropsType> = ({forecasts}:PropsType) => {

    return <Tabs
        defaultActiveKey="1"
        centered
        items={forecasts?.map(forecast => ({
            label: <Typography.Title level={4} style={{ margin: 0 }}>{formatDate(forecast.date)}</Typography.Title>,
            key: forecast.date,
            children: <WeatherCard forecast={forecast}/>,
        }))}
        tabPosition={'left'}
    />
}
export default WeatherList