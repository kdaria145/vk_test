import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import WeatherSlice from '../store/weatherSlice';
import PlacesSlice from '../store/placesSlice';

export const store = configureStore({
    reducer: {
        weather: WeatherSlice,
        places: PlacesSlice,
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
    >;
