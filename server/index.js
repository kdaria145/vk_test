import express from 'express'
import cors from 'cors'
import axios from 'axios'

const PORT = process.env.POST ?? 3001;
const app = express();
const baseURL = 'https://api.weather.yandex.ru/v2/informers';
const key = '7f56de1a-bb4c-4aa1-bdfa-105d221967db';
const lang = 'ru_RU';

const Cash = {
    lat: 0,
    lon: 0,
};

let obj = {};

const setObj = (data) => {
    obj = data
}

let interval;

app.use(cors())

const fetchWeather = async (lat = 55.3333, lon = 86.0833) => {
    await axios.get(`${baseURL}?lat=${lat}&lon=${lon}&lang=${lang}`,
        {
            headers:
                {
                    'X-Yandex-API-Key': key
                }
        })
        .then((res) => {
            setObj(res.data);
        })
        .catch(e => {
            console.log(e)
        });

    clearInterval(interval);

    interval = setInterval(() => {
        axios.get(`${baseURL}?lat=${lat}&lon=${lon}&lang=${lang}`,
            {
                headers: {
                    'X-Yandex-API-Key': key
                }
            })
            .then((res) => {
                setObj(res.data);
            })
            .catch(e => console.log(e));
    }, 3600000)
    return obj
}

app.get('/weather', (req, res) => {
    let params = req.query
    fetchWeather(params.lat, params.lon).then(response => {
        res.json(obj)
    })
})


app.listen(PORT, () => {
    console.log(`server has been started on post ${PORT}`)
})
